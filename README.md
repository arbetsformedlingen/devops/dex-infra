# Dex IDP infra repository

Kustomize deployment script top deploy [Dex IDP](https://dexidp.io/).
Dex IDP is an OpenID Connect provider.

It is configured to enable LDAP login for JobTech products.

In directory [example-app-deployment](example-app-deployment) is
instructions and configuration files to set up authentication for
applications without changing the app-code. It sets up an
authenticating reverse proxy in front of the app.
The app can get information about the user via the HTTP-headers:

* `HTTP_X_FORWARDED_EMAIL` - username (Note! Not email due to LDAP
  setup.)
* `HTTP_X_FORWARDED_GROUP` - List of groups user is member of.
